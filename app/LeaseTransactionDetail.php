<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LeaseTransactionDetail extends Model
{
    protected $table = 'lease_transaction_details';
    protected $fillable = ['lease_transaction_id','product_id','product_quantity','product_price'];

    public function leaseTransactionDetail(){
        return $this->belongsTo('App\LeaseTransaction','lease_transaction_details_id');
    }
    public function product(){
        return $this->belongsTo('App\Product','product_id');
    }
}
