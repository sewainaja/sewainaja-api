<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserBiling extends Model
{
    protected $table = 'user_billings';
    protected $fillable = ['user_id',];
    protected $hidden = ['user_card_number','user_card_expdate','user_card_cvc'];

    public function user(){
        return $this->belongsTo('App\UserBiling','user_id');
    }
}
