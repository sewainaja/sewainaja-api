<?php

namespace App\Http\Controllers;

use App\Http\Requests\Address\AddressStoreRequest;
use App\Http\Requests\Address\AddressUpdateRequest;
use App\Address;
use App\Services\Response;

class AddressController extends Controller
{
    private $addresses;

    public function __construct(Address $addresses){
        $this->addresses = $addresses;
    }

    public function index(){
        $user = auth()->user();
        $data = $this->addresses->where('user_id','=',$user['id'])->get();
        return Response::data($data);
    }

    public function show($id){
        $data = $this->addresses->find($id);
        return Response::data($data);
    }

    public function store(AddressStoreRequest $request){
        $user = auth()->user();

        $params = $request->toArray();
        $params['user_id'] = $user['id'];

        $this->addresses->create($params);

        return Response::message('Address have been added');
    }

    public function update(AddressUpdateRequest $request,$id){
        $params = $request->except('user_id');
        $address = $this->addresses->find($id);
        $address->fill($params);
        $address->save();
        return Response::message('Update address successfull');
    }

    public function destroy($id){
        $address = $this->addresses->find($id);
        $address->delete();
        return Response::message('Delete address succesfull');

    }
}
