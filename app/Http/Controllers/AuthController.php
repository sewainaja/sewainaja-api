<?php

namespace App\Http\Controllers;

use App\Services\Response;
use Illuminate\Http\Request;
// The AuthController is the child of the Controller which functions to type in the username and password to access the transaction
class AuthController extends Controller
{
    public function login(Request $request){
        $credentials = $request->only(['email', 'password']);

        if (! $token = auth()->attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        return Response::data([
            'token' => $token,
        ]);
    }

    public function logout() {
        auth()->logout();

        return Response::message('Log out success!');
    }

    public function me() {
        return Response::data(auth()->user());
    }
}
