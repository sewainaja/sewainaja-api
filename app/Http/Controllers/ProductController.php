<?php

namespace App\Http\Controllers;

use App\Http\Requests\Product\ProductStoreRequest;
use App\Http\Requests\Product\ProductUpdateRequest;
use App\Http\Requests\Product\productsearch;
use App\Product;
use App\LeaseTransactionDetail;
use App\Services\Response;

class ProductController extends Controller
{
    private $product;

    public function __construct(Product $product){
        $this->product = $product;
    }

    public function index(){
        $data = $this->product->with(['productPrices', 'productImages'])->get();

        return Response::data($data);
    }

    public function show($id){
        $data = $this->product->with(['user', 'productPrices','productImages'])->find($id);

        return Response::data($data);
    }

    public function store(ProductStoreRequest $request) {
        $params = $request->toArray();

        $this->product->create($params);

        return Response::message('Create product success');
    }

    public function update(ProductUpdateRequest $request, $id) {
        $params = $request->except('user_id');

        $product = $this->product->find($id);

        $product->fill($params);
        $product->save();

        return Response::message('Update product success');
    }

    public function destroy($id) {
        $product = $this->product->find($id);

        $product->fill([
            'status' => 'blocked'
        ]);
        $product->save();

        return Response::message('Delete product success');
    }

    public function myProduct(){
        $user = auth()->user();
        if ($user['role'] == 'provider'){
            $data = $this->product->with(['productImages','productPrices'])->where('user_id','=',$user['id'])->get();
            return Response::data($data);
        }else{
            return Response::message('Not a provider!');
        }
    }

    public function search(productsearch $request){
        $params = $request->toArray();
        $data = $this->product->with(['productImages','productPrices'])->where('name','like','%'.$params['search'].'%')->get();
        // return Response::message($data);
        return Response::data($data);
    }

    public function MostBought(){
        $data = $this->product->withCount('lease_trans_details')->with(['productImages','productPrices'])->orderBy('lease_trans_details_count','desc')->take(12)->get();
        return Response::data($data);
    }

    public function NewlyCreated(){
        $data = $this->product->with(['productImages','productPrices'])->orderBy('created_at')->take(12)->get();
        return Response::data($data);
    }


}
