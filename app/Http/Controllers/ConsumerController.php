<?php

namespace App\Http\Controllers;

use App\LeaseTransaction;
use App\Services\Response;
use Illuminate\Http\Request;

class ConsumerController extends Controller
{
    private $consumers;

    public function __construct(LeaseTransaction $consumers){
        $this->consumers = $consumers;
    }

    public function index($providerid,$id){
        $data = $this->consumers->find($id);
        return Response::data($data);
    }

    public function show($providerid,$id){
        $data = $this->consumers->where('provider_id', '=', $providerId)->get();
        return Response::data($data);
    }
}
