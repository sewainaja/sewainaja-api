<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductImage\ProductImageStoreRequest;
use App\Http\Requests\ProductImage\ProductImageUpdateRequest;
use App\ProductImages;
use App\Services\Response;

class ProductImageController extends Controller{
	private $image;

	public function __construct(ProductImages $product){
        $this->image = $product;
    }

    public function index(){
        $data = $this->image->all();

        return Response::data($data);
    }

    public function show($id){
        $data = $this->image->find($id);

        return Response::data($data);
    }

    public function store(ProductImageStoreRequest $request) {
        $params = $request->toArray();

        $this->image->create($params);

        return Response::message('Create image success');
    }

    public function update(ProductImageUpdateRequest $request, $id) {
        $params = $request->toArray();

        $image = $this->image->find($id);

        $image->fill($params);
        $image->save();

        return Response::message('Update image success');
    }

    public function destroy($id) {
        $image = $this->image->find($id);

        $image->delete();

        return Response::message('Delete image success');
    }
}
