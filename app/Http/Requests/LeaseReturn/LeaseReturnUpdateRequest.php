<?php

namespace App\Http\Requests\LeaseReturn;

use App\Http\Requests\ApiRequest;
use Illuminate\Foundation\Http\FormRequest;

class LeaseReturnUpdateRequest extends FormRequest
{
    use ApiRequest;
    public function rules()
    {
        return [
            'lease_transaction_id' => 'required|exists:lease_transactions,id'
        ];
    }
}
