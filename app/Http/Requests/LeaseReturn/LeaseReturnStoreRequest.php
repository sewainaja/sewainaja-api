<?php

namespace App\Http\Requests\LeaseReturn;

use Illuminate\Foundation\Http\FormRequest;

use App\Http\Requests\ApiRequest;
class LeaseReturnStoreRequest extends FormRequest
{
    use ApiRequest;
    public function rules()
    {
        return [
            'lease_transaction_id' => 'required|exists:lease_transactions,id',
            'delivery_date' => 'required|date',
            'delivery_reciept' => 'required'
        ];
    }
}
