<?php

namespace App\Http\Requests\LeaseTransaction;

use App\Http\Requests\ApiRequest;
use Illuminate\Foundation\Http\FormRequest;

class LeaseTransactionStoreRequest extends FormRequest
{
    use ApiRequest;

    public function rules()
    {
        return [
            'product_id' => 'required|exists:products,id',
            'quantity' => 'required|integer',
            'address_id' => 'required|exists:address,id',
            'product_price_id' => 'required|exists:product_prices,id',
            'lease_time_recurrence' => 'required|integer',
            'start_date' => 'required|date'
        ];
    }
}
