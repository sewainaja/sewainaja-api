<?php

namespace App\Http\Requests\ProductImage;

use App\Http\Requests\ApiRequest;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ProductImageUpdateRequest extends FormRequest
{
    use ApiRequest;

    public function rules() {
        return [
            'image_description' => 'alpha'
        ];
    }
}
