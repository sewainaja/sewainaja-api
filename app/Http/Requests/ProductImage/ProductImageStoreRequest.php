<?php

namespace App\Http\Requests\ProductImage;

use App\Http\Requests\ApiRequest;
use Illuminate\Foundation\Http\FormRequest;

class ProductImageStoreRequest extends FormRequest
{
    use ApiRequest;

    public function rules() {
        return [
            'product_id' => 'required|integer',
            'image_link' => 'required|integer',
            'image_descriptions' => 'required'

        ];
    }
}
