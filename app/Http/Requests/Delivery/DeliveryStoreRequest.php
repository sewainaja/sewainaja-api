<?php

namespace App\Http\Requests\Delivery;

use App\Http\Requests\ApiRequest;
use Illuminate\Foundation\Http\FormRequest;

class DeliveryStoreRequest extends FormRequest
{

    use ApiRequest;
    public function rules()
    {
        return [
            'address_id' => 'required|exists:address,id',
        ];
    }
}
