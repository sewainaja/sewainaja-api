<?php

namespace App\Http\Requests;

use App\Services\Response;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
// Request that the api needs to authorize based on the following code otherwise it fails when there's an error.
trait ApiRequest
{
    public function authorize() {
        return true;
    }

    protected function failedValidation(Validator $validator) {
        throw new HttpResponseException(Response::validationError($validator->errors()));
    }
}
