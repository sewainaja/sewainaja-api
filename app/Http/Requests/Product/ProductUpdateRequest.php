<?php

namespace App\Http\Requests\Product;

use App\Http\Requests\ApiRequest;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ProductUpdateRequest extends FormRequest
{
    use ApiRequest;

    public function rules() {
        return [
            'stock' => 'integer',
            'penalty_fee' => 'integer',
            'status' => Rule::in([
                'alive', 'blocked', 'suspended'
            ])
        ];
    }
}
