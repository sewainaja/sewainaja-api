<?php

namespace App\Http\Requests\Product;

use App\Http\Requests\ApiRequest;
use Illuminate\Foundation\Http\FormRequest;

class productsearch extends FormRequest{

    use ApiRequest;
    public function rules()
    {
        return [
            'search' => 'required'
        ];
    }
}
