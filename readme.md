#Sewainaja!
![Project Logo](https://gitlab.com/sewainaja/sewainaja-app/raw/master/src/assets/Pictures/SewainAjaLogo.png)
Start Sewainaja!

## About
This project is for creating a system for renting.
Some of us regret after buying something. This solve it since you can rent it before you buy it.
The project is live on [sewainaja.me](https://sewainaja.me)

## How to set-up using php laravel & nginx
1. Set up your server 
2. Installing nginx, composer, & github (ubuntu)
'''
$sudo apt get update
$sudo apt get upgrade
$sudo apt get nginx
$sudo apt get composer
$sudo apt get git
'''
3. Change your director to the nginx root file
'''
$cd /var/www/html/
'''
4. Clone from Github/Gitlab using git
'''
$git clone https://gitlab.com/sewainaja/sewainaja-api
'''
* Update using git pull
'''
$cd sewainaja-api
$git pull
'''
5. Install Composer
'''
$composer install laravel
'''
6. Copy .env.example to .env
'''
$cd sewainaja-api
$cp .env.example .env
'''
7. Edit in .env to the right database settings
'''
$nano .env
'''
Set Up Database IP Address,Database Userame, Database Name, Database Password
* Set-Up Database 
'''
$sudo apt get install mysql-server
$service mysql-server start
$mysql -u root
'''
In Mysql Server
'''
$GRANT ALL PRIVILEGES ON *.* TO 'username'@'localhost' IDENTIFIED BY 'password';
$Create Database Database-Name;
'''
8. Generate Laravel Key
'''
$php artisan key:generate
'''
9. Set Up Nginx & Cloudflare
'''
$cd /etc/nginx/sites-available/
$rm default
$nano website-name
'''
* Set up listen port 80 and port 443+ssl in the website-name
* Set up location & root folder, In root direct it to /var/www/html/sewainaja-api/public, In index change it to index.php 
10. Link sites-available to sites-enabled and remove default from sites-enabled
'''
$rm default
$ln -d ~/etc/nginx/sites-available/website-name ~/etc/nginx/sites-enabled/
'''

11. Restart the service
'''
$service nginx restart
or
$service nginx reload
or
etc
'''

## How to use the api
Note :  Ever api link start with WEBSITES-URL/api/~ 
1. Login (JWT)
'''
Post Method
~/auth/login
'''
> Request
>> Username
>> Password
2. Logout (JWT)
'''
Get Method
~/auth/logout
'''
3. My Details (JWT)
'''
GET/HEAD Method
~/auth/me
'''
4. Register (JWT)
New User Registration
'''
Post Method
~/auth/register
'''
> Request
>> name
>> email
>> password
>> password verification
5. Deliveries (JWT)
'''
POST Method
~/deliveries
> Request
>> address_id
'''
'''
GET/HEAD Method
~/deliveries
'''
'''
DELETE Method
~/deliveries/{id}
'''
'''
PUT Method
~/deliveries/{id}
> Request
>> delivery_date
>> delivery_receipt
'''
6. Lease Checkout (JWT)
Checkout happends when you would like to rent
'''
POST Method
~/lease/checkout
> Request
>> product_id
>> quantity
>> address_id
>> product_price_id
>> lease_time_recurrence
>> start_date
'''
7. Lease Delivery (JWT)
Add receipt id to the deliveries
'''
POST Method
~/lease/delivery
> Request
>> transaction_id
>> receipt_no
'''
8. Lease Pay (JWT)
Update Transaction Status
'''
POST Method
~/lease/pay
> Request
>> transaction_id
'''
9. Lease Previous Transaction (JWT)
Consumer
'''
GET Method
~/lease/transactions-consumer
'''
Provider
'''
GET Method
~/lease/transactions-provider
'''
10. Lease Transaction Status (JWT)
'''
GET Method
~/lease/transactions-status
'''
11. Check Other API Using
'''
$sudo php artisan route:list
'''





