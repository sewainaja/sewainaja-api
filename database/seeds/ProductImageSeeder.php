<?php

use Illuminate\Database\Seeder;

use App\ProductImages;

class ProductImageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();
        for ($i = 1; $i <= 50; $i++){
            for ($j = 0; $j < 4; $j++) {
                ProductImages::create([
                    'product_id' => $i,
                    'image_link' => 'storage/images/' . rand(1, 12) . '.jpg',
                    'image_descriptions' => $faker->sentence
                ]);
            }
        }
    }
}
