<?php

use Illuminate\Database\Seeder;
use App\Address;

class AddressTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();
        for ($i =0; $i <50; $i++){
            Address::create([
                'user_id' => rand(1,50),
                'province' => $faker->state,
                'city' => $faker->city,
                'zipcode' => $faker->postcode,
                'address' => $faker->address,
            ]);
        }
    }
}
