<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Delivery;

class DeliveryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Delivery::truncate();
        $faker = \Faker\Factory::create();
        for ($i =0; $i <50; $i++){
            Delivery::create([
                'delivery_date' => $faker->dateTime(),
                'address_id' =>rand(1,50),
                'delivery_reciept' => $faker->asciify('********************')
            ]);
        }
    }
}
